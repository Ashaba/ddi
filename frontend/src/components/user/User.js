import React from 'react';
import Modal from 'react-foundation-modal';
import { Link } from "react-router-dom";
import './User.css'

const overlayStyle = {
    'backgroundColor': 'rgba(33,10,10,.45)'
    };

class UserDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalIsOpen: false,
            postcode: '',
            street: '',
            city: '',
            state: '',
            latitude: '',
            longitude: '',
            offset: '',
            description: '',
            surname: '',
            dob: ''
         }
         this.handleChange = this.handleChange.bind(this);
         this.update = this.update.bind(this);
    }
     showPopup = (status) => {
        this.setState({
            modalIsOpen: status
        });
    }

    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    async update(event) {
        event.preventDefault();
        //prepare payload to update address
        console.log("I am updating", this.state.postcode)
        this.setState({
            modalIsOpen: false
        });
    }

    componentDidMount = () => {
        this.setState({
            postcode: this.props.location.state.address.postcode,
            street: this.props.location.state.address.street,
            city: this.props.location.state.address.city,
            state: this.props.location.state.address.state,
            latitude: this.props.location.state.address.latitude,
            longitude: this.props.location.state.address.longitude,
            offset: this.props.location.state.address.timezone_offset,
            description: this.props.location.state.address.timezone_description,
            surname: this.props.location.state.surname,
            dob: this.props.location.state.dob
        })
    }

    render() {
        return (
            <div className="card-profile-stats">
                <div className="card-profile-stats-intro">
                    <div className="card-profile-stats-intro-content" id="welcome-position">
                        <h3>Welcome {this.state.surname}</h3>
                        <center><small><p>{this.state.dob}</p></small></center>
                        <center><p><Link to="/">Logout</Link></p></center>
                    </div>
                </div>
                <hr />
                <div className="card-profile-stats-container">
                    <div className="card-profile-stats-statistic">
                        <h3>User Address</h3>
                        <p><b>Post Code:</b> {this.state.postcode}</p>
                        <p><b>Street:</b> {this.state.street}</p>
                        <p><b>City:</b> {this.state.city}</p>
                        <p><b>State:</b> {this.state.state}</p>
                        <p><b>Latitude:</b> {this.state.latitude}</p>
                        <p><b>Longitude:</b> {this.state.longitude}</p>
                        <p><b>Offset:</b> {this.state.offset}</p>
                        <p><i>{this.state.description}</i></p>
                        <input type="submit" onClick={() => this.showPopup(true)} className="button expanded" value="Update" />
                    </div>
                </div>
                
                <Modal 
                    open={this.state.modalIsOpen}
                    closeModal={this.showPopup}
                    isModal={true}
                    size="medium"
                    overlayStyle={overlayStyle} >
                    <h4>Update User Address</h4>
                    <form className="log-in-form" onSubmit={this.update}>
                    <div className="grid-x grid-padding-x">
                        <div className="small-3 cell">
                            <label htmlFor="right-label" className="text-right">Post Code</label>
                        </div>
                        <div className="small-9 cell">
                            <input type="text" name="postcode" value={this.state.postcode} onChange={this.handleChange} id="right-label"/>
                        </div>
                        <div className="small-3 cell">
                            <label htmlFor="right-label" className="text-right">Street</label>
                        </div>
                        <div className="small-9 cell">
                            <input type="text" name="street" value={this.state.street} onChange={this.handleChange} id="right-label"/>
                        </div>
                        <div className="small-3 cell">
                            <label htmlFor="right-label" className="text-right">City</label>
                        </div>
                        <div className="small-9 cell">
                            <input type="text" name="city" value={this.state.city} onChange={this.handleChange} id="right-label"/>
                        </div>
                        <div className="small-3 cell">
                            <label htmlFor="right-label" className="text-right">State</label>
                        </div>
                        <div className="small-9 cell">
                            <input type="text" name="state" value={this.state.state} onChange={this.handleChange} id="right-label"/>
                        </div>
                        <div className="small-3 cell">
                            <label htmlFor="right-label" className="text-right">Longitude</label>
                        </div>
                        <div className="small-9 cell">
                            <input type="text" name="longitude" value={this.state.longitude} onChange={this.handleChange} id="right-label"/>
                        </div>
                        <div className="small-3 cell">
                            <label htmlFor="right-label" className="text-right">Latitude</label>
                        </div>
                        <div className="small-9 cell">
                            <input type="text" name="latitude" value={this.state.latitude} onChange={this.handleChange} id="right-label"/>
                        </div>
                        <div className="small-3 cell">
                            <label htmlFor="right-label" className="text-right">Offset</label>
                        </div>
                        <div className="small-9 cell">
                            <input type="text" name="offset" value={this.state.offset} onChange={this.handleChange} id="right-label"/>
                        </div>
                        <div className="small-3 cell">
                            <label htmlFor="right-label" className="text-right">Description</label>
                        </div>
                        <div className="small-9 cell">
                            <input type="text" name="description" value={this.state.description} onChange={this.handleChange} id="right-label"/>
                        </div>
                        <input type="submit" className="button" value="Update" />
                    </div>
                </form> 
                </Modal> 
            </div>
        );
    }
}

export default UserDetail;
