
// const DEVELOPMENTBASEURL = 'http://127.0.0.1:5000';
const PRODUCTIONBASEURL = 'https://ddi-api.herokuapp.com'

export const loginUser = async (email, password) => {
    const URL = `${PRODUCTIONBASEURL}/api/v1/auth/login`;
    try {
        const response = await fetch(URL, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: `${email}`,
                password: `${password}`
            }),
        });
        const responseJson = await response.json();
        return responseJson;
    } catch(error) {
        return error;
    }
};

export const updateAddress = async (payload) => {
    //implement a call to the backendbefore updating the user address
}
