import os

# Define the application directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

secret_key = os.urandom(24)

POSTGRES = {
    'user': 'ash',
    'password': 'py',
    'database_name': 'ddi',
    'host': 'localhost',
    'port': '5432',
}

POSTGRES_TEST = {
    'user': 'ash',
    'password': 'py',
    'database_name': 'ddi_test',
    'host': 'localhost',
    'port': '5432',
}

postgres_local_base = 'postgresql://%(user)s:%(password)s@%(host)s:%(port)s/%(database_name)s' % POSTGRES
postgres_test = 'postgresql://%(user)s:%(password)s@%(host)s:%(port)s/%(database_name)s' % POSTGRES_TEST


class Config(object):
    """ Base configuration."""

    # Secret key for signing cookies
    SECRET_KEY = os.getenv('SECRET_KEY', secret_key)

    BCRYPT_LOG_ROUNDS = 13

    SQLALCHEMY_TRACK_MODIFICATIONS = False

    DEBUG = False

    TESTING = False

    THREADS_PER_PAGE = 2

    # Enable protection agains *Cross-site Request Forgery (CSRF)*

    CSRF_ENABLED = True

    # Use a secure, unique and absolutely secret key for
    # signing the data.

    CSRF_SESSION_KEY = "secret"


class ProductionConfig(Config):
    SECRET_KEY = secret_key
    DEBUG = False
    # SQLALCHEMY_DATABASE_URI = str(os.environ.get('DATABASE_URL'))
    SQLALCHEMY_DATABASE_URI = "postgres://uzmcbjloykrhde:94ec8375d43e7b7ba87828b768ebeb35d9c2c21d6facf196e7b7e37e35b7542e@ec2-54-221-238-248.compute-1.amazonaws.com:5432/d58legq42roote"


class DevelopmentConfig(Config):
    """Development configuration."""
    DEBUG = True
    BCRYPT_LOG_ROUNDS = 4
    SQLALCHEMY_DATABASE_URI = postgres_local_base


class TestingConfig(Config):
    """Testing configuration."""
    DEBUG = True
    TESTING = True
    BCRYPT_LOG_ROUNDS = 4
    SQLALCHEMY_DATABASE_URI = postgres_test
    PRESERVE_CONTEXT_ON_EXCEPTION = False
