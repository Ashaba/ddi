import requests
import logging
from backend.src import db, app, bcrypt
from backend.src.api import User, Picture, Credential, Address


def fetch_users():
    try:
        request = requests.get('https://randomuser.me/api/')
        users_list = request.json()["results"]
        return users_list
    except Exception as e:
        logging.error("Failed to fetch users from the server, ", e)


def insert_users():
    """This could be better implemented using a background process, eg celery"""
    users = fetch_users()
    try:
        for user in users:
            user_available = User.query.filter_by(email=user['email']).first()
            print(user['login']['password'], user['email'])
            if not user_available:
                password = bcrypt.generate_password_hash(user['login']['password'],
                                                         app.config['BCRYPT_LOG_ROUNDS']).decode()

                picture_object = Picture(large=user['picture']['large'], medium=user['picture']['medium'],
                                         thumbnail=user['picture']['thumbnail'])

                credential_object = Credential(uuid=user['login']['uuid'], username=user['login']['username'],
                                               password=password, salt=user['login']['salt'],
                                               md5=user['login']['md5'], sha1=user['login']['sha1'],
                                               sha256=user['login']['sha256'])

                address_object = Address(postcode=user['location']['postcode'], street=user['location']['street'],
                                         city=user['location']['city'], state=user['location']['state'],
                                         latitude=user['location']['coordinates']['latitude'],
                                         longitude=user['location']['coordinates']['longitude'],
                                         timezone_offset=user['location']['timezone']['offset'],
                                         timezone_description=user['location']['timezone']['description'])

                user_object = User(gender=user['gender'], title=user['name']['title'], first_name=user['name']['first'],
                                   last_name=user['name']['last'], email=user['email'], dob=user['dob']['date'],
                                   age=user['dob']['age'], phone=user['phone'], cell=user['cell'],
                                   id_name=user['id']['name'],
                                   id_value=user['id']['value'], registered_date=user['registered']['date'],
                                   registration_age=
                                   user['registered']['age'], nat=user['nat'], picture=picture_object,
                                   credential=credential_object, address=address_object)

                db.session.add(user_object)
                db.session.commit()

    except Exception as e:
        logging.error(e)
