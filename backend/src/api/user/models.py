from flask_restful_swagger import swagger

from backend.src import db


class Base(db.Model):
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(db.DateTime, default=db.func.current_timestamp(), onupdate=db.func.current_timestamp())


@swagger.model
class User(Base):
    """User Model"""

    __tablename__ = 'user'
    __table_args__ = {'extend_existing': True}

    gender = db.Column(db.String(128), nullable=True)
    title = db.Column(db.String(128), nullable=True)
    first_name = db.Column(db.String(128), nullable=True)
    last_name = db.Column(db.String(128), nullable=True)
    email = db.Column(db.String(128), nullable=False, unique=True)
    dob = db.Column(db.DateTime, nullable=True)
    age = db.Column(db.Integer)
    phone = db.Column(db.String(128))
    cell = db.Column(db.String(128))
    id_name = db.Column(db.String(128), nullable=True)
    id_value = db.Column(db.String(128), nullable=True)
    registered_date = db.Column(db.DateTime, nullable=True)
    registration_age = db.Column(db.Integer)
    nat = db.Column(db.String(50), nullable=True)
    picture = db.relationship('Picture', uselist=False, backref='user_picture')
    credential = db.relationship('Credential', uselist=False, backref='credential')
    address = db.relationship('Address', uselist=False, backref='address')

    @property
    def serialize(self):
        # will need to add the related models in the serializer
        return {
            'id': self.id,
            'date_created': self.date_created,
            'date_modified': self.date_modified,
            'gender': self.gender,
            'title': self.title,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'email': self.email,
            'dob': self.dob,
            'age': self.age,
            'id_value': self.id_value,
            'nat': self.nat
        }


class Picture(db.Model):
    """Picture model for storing user thumbnails"""
    __tablename__ = 'user_picture'

    id = db.Column(db.Integer, primary_key=True)
    large = db.Column(db.String(500), unique=False, nullable=True)
    medium = db.Column(db.String(500), unique=False, nullable=True)
    thumbnail = db.Column(db.String(500), unique=False, nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __str__(self):
        return self.thumbnail

    @property
    def serialize(self):
        return {
            'id': self.id,
            'large': self.large,
            'medium': self.medium,
            'thumbnail': self.thumbnail
        }


class Credential(db.Model):
    """Credential model to store user credentials"""
    __tablename__ = 'user_credential'

    uuid = db.Column(db.String(128), nullable=False, primary_key=True)
    username = db.Column(db.String(128), nullable=False)
    password = db.Column(db.String(255), nullable=False)
    salt = db.Column(db.String(128), nullable=False)
    md5 = db.Column(db.String(500), nullable=False)
    sha1 = db.Column(db.String(500), nullable=False)
    sha256 = db.Column(db.String(500), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __str__(self):
        return self.uuid

    @property
    def serialize(self):
        return {
            'uuid': self.uuid,
            'username': self.username,
        }


class Address(db.Model):
    """Address Model to store user location details"""
    __tablename__ = 'user_address'

    postcode = db.Column(db.String(128), nullable=False, primary_key=True)
    street = db.Column(db.String(128), nullable=True)
    city = db.Column(db.String(128), nullable=True)
    state = db.Column(db.String(128), nullable=True)
    latitude = db.Column(db.Float, nullable=True)
    longitude = db.Column(db.Float, nullable=True)
    timezone_offset = db.Column(db.String(128), nullable=True)
    timezone_description = db.Column(db.String(128), nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __str__(self):
        return self.postcode

    @property
    def serialize(self):
        return {
            'user_id': self.user_id,
            'postcode': self.postcode,
            'street': self.street,
            'state': self.state,
            'latitude': self.latitude,
            'longitude': self.longitude,
            'offset': self.timezone_offset,
            'timezone_description': self.timezone_description
        }
