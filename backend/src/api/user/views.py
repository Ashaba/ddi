from flask import jsonify, make_response
from flask_restful import Api, Resource, reqparse
from flask_restful_swagger import swagger

from backend.src import app, bcrypt
from backend.src.api import User
from backend.src.api.user import services

from validate_email import validate_email

api = swagger.docs(Api(app), apiVersion='1')
parser = reqparse.RequestParser()

login_parser = reqparse.RequestParser()
login_parser.add_argument('email', required=True, help='Email address is required')
login_parser.add_argument('password', required=True, help='Password is required')


class HeartBeat(Resource):
    """Checking the status of the api"""

    def get(self):
        services.insert_users()
        response_object = {
            'success': 'ok',
            'message': "DDI API is working"
        }
        return make_response(jsonify(response_object), 200)


class AuthenticateUser(Resource):
    """Authenticate user using email address and password"""

    @swagger.operation(
        notes='End point to authenticate User',
        responseClass=User.__name__,
        nickname='login',
        parameters=[
            {
                "name": "email",
                "description": "email address - unique",
                "required": True,
                "allowMultiple": False,
                "dataType": User.__name__,
                "paramType": "email"
            },
            {
                "name": "password",
                "description": "enter password",
                "required": True,
                "allowMultiple": False,
                "dataType": User.__name__,
                "paramType": "password"
            }
        ]
    )
    def post(self):
        args = login_parser.parse_args()
        email, password = args.email, args.password
        if not validate_email(email):
            response_object = {
                'error': 'failed to authenticate user',
                'message': '{} Is not a valid email address. Please Try again!'.format(email)
            }
            return make_response(jsonify(response_object), 404)
        try:
            user = User.query.filter_by(email=email).first()
            if user and bcrypt.check_password_hash(user.credential.password, password):
                user_uuid = str(user.credential.uuid)
                if user_uuid:
                    response_object = {
                        'success': 'ok',
                        'message': 'Successfully logged in',
                        'id': user.id,
                        'uuid': user.credential.uuid,
                        'nat': user.nat,
                        'phone': user.phone,
                        'name': user.credential.username,
                        'dob': user.dob,
                        'surname': user.last_name,
                        'address': user.address.serialize
                    }
                    return make_response(jsonify(response_object), 200)
            else:
                response_object = {
                    'error': 'failed to authenticate user',
                    'message': 'Incorrect email or password, Try again!'
                }
                return make_response(jsonify(response_object), 404)
        except Exception as e:
            response_object = {
                'error': 'failed to authenticate user',
                'message': str(e)
            }
            return make_response(jsonify(response_object), 401)


class UserResource(Resource):
    """A resource for handling user operations"""

    @swagger.operation(
        notes='Fetch All Users',
        responseClass=User.__name__,
        nickname='view Users'
    )
    def get(self):
        users = User.query.order_by(User.id).all()
        if len(users) > 0:
            results = [i.serialize for i in users]
            response_object = {
                'success': 'ok',
                'users': results
            }
            return make_response(jsonify(response_object))

        else:
            response_object = {
                'error': 'failed to fetch users',
                'message': 'No users found'
            }
            return make_response(jsonify(response_object), 404)


class UserDetail(Resource):
    """User detail"""

    @swagger.operation(
        notes='End point to view a user',
        responseClass=User.__name__,
        nickname='User detail'
    )
    def get(self, user_id):
        try:
            user = User.query.filter_by(id_value=user_id).first()
            response_object = {
                'success': 'ok',
                'user': user.serialize
            }
            return make_response(jsonify(response_object), 200)
        except Exception as e:
            response_object = {
                'error': 'failed to get user details',
                'message': 'User with Id {} does not exist'.format(user_id)
            }
            return make_response(jsonify(response_object), 404)


class UserAddress(Resource):
    """Users address"""

    @swagger.operation(
        notes='End point to view a users address',
        responseClass=User.__name__,
        nickname='User Address'
    )
    def get(self, user_id):
        try:
            user = User.query.filter_by(id_value=user_id).first()
            response_object = {
                'success': 'ok',
                'address': user.address.serialize
            }
            return make_response(jsonify(response_object), 200)
        except Exception as e:
            response_object = {
                'error': 'fail',
                'message': 'Address of user with Id {} does not exist'.format(user_id)
            }
            return make_response(jsonify(response_object), 404)


api.add_resource(HeartBeat, '/')
api.add_resource(AuthenticateUser, '/api/v1/auth/login')
api.add_resource(UserResource, '/api/v1/users')
api.add_resource(UserDetail, '/api/v1/users/<user_id>')
api.add_resource(UserAddress, '/api/v1/users/<user_id>/address')