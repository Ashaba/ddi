## DDI API ##

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)


## Requirements

* Python3+
* Postgres Database Engine

## Installation

1. Clone the repository:

    >`$ git clone git@bitbucket.org:Ashaba/ddi.git`

2. Create and activate a virtual environment and install the dependencies

    `$ pip install -r requirements.txt`

3. Configure the environment

    update `POSTGRES` and `POSTGRES_TEST` in the `config.py` to match your database credentials

4. Update the database tables

    `$ python app.py db upgrade`


## Run the application

  Run the server `python app.py runserver`.

  You should be able to browse the api documentation here `/api/spec.html`
