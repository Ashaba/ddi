from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from backend.src import app, db
migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)


@manager.command
def create_db():
    """Creates the database tables for the ddi app"""
    db.create_all()


@manager.command
def drop_db():
    """Drops the database tables"""
    db.drop_all()


if __name__ == '__main__':
    manager.run()
